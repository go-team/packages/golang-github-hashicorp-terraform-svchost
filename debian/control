Source: golang-github-hashicorp-terraform-svchost
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any (>= 2:1.19~),
               golang-github-google-go-cmp-dev (>= 0.5.9),
               golang-github-hashicorp-go-cleanhttp-dev (>= 0.5.2),
               golang-github-hashicorp-go-version-dev (>= 1.6.0),
               golang-github-zclconf-go-cty-dev (>= 1.12.1),
               golang-golang-x-net-dev (>= 1:0.5.0),
               golang-golang-x-oauth2-google-dev (>= 0.4.0)
Standards-Version: 4.6.2
Homepage: https://github.com/hashicorp/terraform-svchost
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-hashicorp-terraform-svchost
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-hashicorp-terraform-svchost.git
XS-Go-Import-Path: github.com/hashicorp/terraform-svchost
Testsuite: autopkgtest-pkg-go

Package: golang-github-hashicorp-terraform-svchost-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-hashicorp-go-cleanhttp-dev (>= 0.5.2),
         golang-github-hashicorp-go-version-dev (>= 1.6.0),
         golang-github-zclconf-go-cty-dev (>= 1.12.1),
         golang-golang-x-net-dev (>= 1:0.5.0),
         golang-golang-x-oauth2-google-dev (>= 0.4.0),
         ${misc:Depends}
Description: handling of friendly hostnames for terraform
 This package deals with the representations of the so-called "friendly
 hostnames" that are used to represent systems that provide Terraform-native
 remote services, such as module registry, remote operations, etc.
 .
 Friendly hostnames are specified such that, as much as possible, they
 are consistent with how web browsers think of hostnames, so that users
 can bring their intuitions about how hostnames behave when they access
 a Terraform Enterprise instance's web UI (or indeed any other website)
 and have this behave in a similar way.
