module github.com/hashicorp/terraform-svchost

go 1.19

require (
	github.com/google/go-cmp v0.5.9
	github.com/hashicorp/go-cleanhttp v0.5.2
	github.com/hashicorp/go-version v1.6.0
	github.com/zclconf/go-cty v1.12.1
	golang.org/x/net v0.5.0
	golang.org/x/oauth2 v0.4.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
